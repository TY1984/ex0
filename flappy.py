"""Flappy, game inspired by Flappy Bird.

Exercises

1. Keep score.
2. Vary the speed.
3. Vary the size of the balls.
4. Allow the bird to move forward and back.

"""

from turtle import *
from freegames import vector
from random import *

writer = Turtle(visible=False)
state = {'score': 0}
bird = vector(0, 0)
balls = []


class Ball:
    """
    In order to be able to control each and every ball I had to build a class Ball and assign its variables
    upon ball creation
    """
    def __init__(self, ball_speed, ball_size, y):
        self.speed = ball_speed
        self.size = ball_size
        self.vector = vector(199, y)


def up_arrow():
    """Up key will cause the bird to "jump" 30 in y axis and add 1 point to the score"""
    up = vector(0, 30)
    bird.move(up)
    state['score'] += 1


def left_arrow():
    """Left key will cause the bird to "jump" -10 backwards in x axis and reduce 1 point from the score"""
    up = vector(-10, 0)
    bird.move(up)
    state['score'] -= 1


def right_arrow():
    """Right key will cause the bird to "jump" 10 forward in x axis and add 10 point to the score"""
    up = vector(10, 0)
    bird.move(up)
    state['score'] += 10


def inside(point):
    """Return True if point on screen."""
    return -200 < point.x < 200 and -200 < point.y < 200


def draw(alive):
    """Draw screen objects."""
    clear()
    goto(bird.x, bird.y)
    if alive:
        writer.undo()
        dot(10, 'green')
        writer.color("Orange")
        writer.write("Score: " + str(state['score']))
    else:
        writer.undo()
        dot(10, 'red')
        writer.color("red")
        writer.write("Game over with: " + str(state['score']))
        for ball in balls:
            ball.size = 0
    for ball in balls:
        goto(ball.vector.x, ball.vector.y)
        dot(ball.size, 'black')
    update()


def move():
    """Update object positions."""
    bird.y -= 5                         # gravity speed
    for ball in balls:
        ball.vector.x -= ball.speed     # control ball speed
    if randrange(10) == 0:              # Adding a ball to the screen as soon as rand=0
        y = randrange(-199, 199)
        size = randint(10, 50)
        speed = randint(3, 10)
        ball = Ball(speed, size, y)
        balls.append(ball)
    while len(balls) > 0 and not inside(balls[0].vector):  # pops a ball from the array(reached left side of the screen
        balls.pop(0)
    if not inside(bird):
        draw(False)
        return
    for ball in balls:
        if abs(ball.vector - bird) < 15:
            draw(False)
            return
    draw(True)
    ontimer(move, 50)


setup(420, 420, 1500, 0)
hideturtle()
up()
tracer(False)
onkey(up_arrow, "Up")
onkey(left_arrow, "Left")
onkey(right_arrow, "Right")
listen()
move()
done()
